﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CWFinal.Data;
using Microsoft.AspNetCore.Mvc;
using CWFinal.Models;
using CWFinal.Models.ManageViewModels;
using CWFinal.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace CWFinal.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly FileUploadService _fileUploadService;
        private readonly IHostingEnvironment _environment;
        private readonly PagingService _paging;

        public HomeController(ApplicationDbContext context,
            FileUploadService fileUploadService,
            IHostingEnvironment environment,
            PagingService paging)
        {
            _context = context;
            _fileUploadService = fileUploadService;
            _environment = environment;
            _paging = paging;
        }

        public IActionResult Index(int page = 1)
        {
            IEnumerable<Building> model = _context.Buildings;
            PagedObject<Building> pagedObject = _paging.DoPage(model.AsQueryable(), page);

            BuildingPagingViewModel viewModel = new BuildingPagingViewModel
            {
                PageViewModel = new PageViewModel(pagedObject.Count, page, pagedObject.PageSize),
                Buildings = pagedObject.Objects,
            };

            return View(viewModel);
        }

        public IActionResult AddBuilding(string message)
        {
            ViewBag.Message = message;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddBuilding(BuildingRegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (await TrueOrFalseUniqeBuildingName(model.Name))
                {
                    ApplicationUser user = _context.Users.FirstOrDefault(u => u.UserName == model.UserName);
                    _context.Add(new Building() { Name = model.Name, Description = model.Description, UserId = user.Id});
                    _context.SaveChanges();
                    Building building = _context.Buildings.FirstOrDefault(g => g.Name == model.Name);

                    building.BuildingLogo = Upload(building.Id, model.BuildingLogo, "images", "avatar");
                    

                    _context.Buildings.Update(building);
                    await _context.SaveChangesAsync();
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    return View(model);
                }
            }
            return View(model);        
        }

        private string Upload(int buildingId, IFormFile file, string folder, string secondFolder)
        {
            var path = Path.Combine(
                _environment.WebRootPath,
                $"{folder}\\{secondFolder}\\{buildingId}");
            _fileUploadService.Upload(path, file.FileName, file);

            return $"{folder}/{secondFolder}/{buildingId}/{file.FileName}";
        }


        [NonAction]
        private async Task<bool> TrueOrFalseUniqeBuildingName(string Name)
        {
            Building building = await _context.Buildings.FirstOrDefaultAsync(u => u.Name == Name);
            if (building == null)
            {
                return true;
            }
            else
            {
                ModelState.AddModelError(String.Empty, $"Заведение с названием {Name} уже существует");
                return false;
            }
        }

        public IActionResult ShowBuildingInfo(int id)
        {
            Building building = _context.Buildings.FirstOrDefault(b => b.Id == id);
            IEnumerable<Picture> pictures = _context.Pictures.Where(p => p.BuildingId == building.Id);
            IEnumerable<Post> posts = _context.Posts.Where(p => p.BuildingId == building.Id);
            ShowBuildingViewModel model = new ShowBuildingViewModel
            {
                Building = building,
                PostList = posts,
                PictureList = pictures
            };
            return View(model);
        }



        public IActionResult AddPost(ShowBuildingViewModel model)
        {
            ApplicationUser user = _context.Users.FirstOrDefault(u => u.UserName == model.UserName);
            Post post = new Post
            {
                PostDescription = model.Message,
                DateTime = DateTime.Now,
                UserId = user.Id,
                BuildingId = model.BuildingId
            };
            Degree degree = new Degree
            {
                Value = model.DegreeValue,
                BuildingId = model.BuildingId,
                UserId = user.Id
            };
            _context.Posts.Update(post);
            _context.Degrees.Update(degree);
            _context.SaveChanges();
            return RedirectToAction("ShowBuildingInfo", "Home" ,new {id = model.BuildingId});
        }

        public IActionResult AddPicture(ShowBuildingViewModel model)
        {
            ApplicationUser user = _context.Users.FirstOrDefault(u => u.UserName == model.UserName);
            Picture picture = new Picture
            {
                ImagePath = Upload(model.BuildingId, model.PicturePath, "images", "avatar"),
                UserId = user.Id,
                BuildingId = model.BuildingId
            };

            _context.Pictures.Update(picture);
            _context.SaveChanges();
            return RedirectToAction("ShowBuildingInfo", "Home", new { id = model.BuildingId });
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
