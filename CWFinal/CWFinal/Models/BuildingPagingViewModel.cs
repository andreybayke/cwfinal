﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CWFinal.Models.ManageViewModels;

namespace CWFinal.Models
{
    public class BuildingPagingViewModel
    {
        public IEnumerable<Building> Buildings { get; set; }
        public PageViewModel PageViewModel { get; set; }
    }
}
