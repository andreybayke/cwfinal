﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace CWFinal.Models
{
    public class BuildingRegisterViewModel
    {
        [Required(ErrorMessage = "Укажите название заведения")]
        [Display(Name = "Название заведения")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Нужно описание заведения. Что это? Морг, ресторан, министерство, бордель...")]
        [Display(Name = "Описание")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Выберите лого")]
        [Display(Name = "Лого заведения")]
        public IFormFile BuildingLogo { get; set; }

        public string UserName {get; set; }
    }
}
