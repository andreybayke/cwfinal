﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CWFinal.Models
{
    public class Degree
    {
        public int Id { get; set; }
        public int Value { get; set; }

        public int BuildingId { get; set; }
        public Building Building { get; set; }

        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
    }
}
