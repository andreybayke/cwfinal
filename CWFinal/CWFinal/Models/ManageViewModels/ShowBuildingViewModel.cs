﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace CWFinal.Models.ManageViewModels
{
    public class ShowBuildingViewModel
    {
        public Building Building { get; set; }
        public IEnumerable<Post> PostList { get; set; }
        public IEnumerable<Picture> PictureList { get; set; }
        public IEnumerable<Degree> DegreeList { get; set; }

        public Degree Degree { get; set; }
        public Post Post { get; set; }
        public Picture Picture { get; set; }
        public IFormFile PicturePath { get; set; }

        public string UserName { get; set; }
        public string Message { get; set; }
        public int BuildingId { get; set; } 
        public int DegreeValue { get; set; }
    }
}
