﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CWFinal.Models
{
    public class Post
    {
        public int Id { get; set; }
        public string PostDescription { get; set; }
        public int PostValue { get; set; }

        public string UserId { get; set; }
        public ApplicationUser User { get; set; }

        public int BuildingId { get; set; }
        public Building Building { get; set; }

        public DateTime DateTime { get; set; }
    }
}
